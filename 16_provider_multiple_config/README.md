# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).
- For more information on multiple provider configurations, please refer [here](https://www.terraform.io/docs/language/providers/configuration.html).

# Commands
- Please refer [here](../01_first-ec2/README.md).