variable "aws_region" {
  // value can be overridden with env var TF_VAR_aws_region
  default = "eu-central-1"
  type    = string
}

variable "aws_access_key" {
  // override value with env var TF_VAR_aws_access_key
  default = null
  type    = string
}

variable "aws_secret_key" {
  // override value with env var TF_VAR_aws_secret_key
  default = null
  type    = string
}

variable "sg_ingress_ports" {
  type        = list(string)
  description = "List of ingress ports"
  default     = [8200, 8201, 8300, 9200, 9500]
}

variable "sg_egress_ports" {
  type        = list(string)
  description = "List of ingress ports"
  default     = [80, 443, 8443]
}