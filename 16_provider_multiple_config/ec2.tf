resource "aws_instance" "eu10-ec2" {
  ami           = "ami-0db9040eb3ab74509"
  instance_type = "t2.micro"
  tags = {
    Name      = "eu10-ec2-instance"
    terraform = "true"
  }
}

resource "aws_instance" "eu20-ec2" {
  ami           = "ami-0fbec3e0504ee1970"
  instance_type = "t2.micro"
  tags = {
    Name      = "my-eu20-ec2-instance"
    terraform = "true"
  }
  provider = aws.my-eu20
}

resource "aws_instance" "us10-ec2-profile" {
  ami = "ami-0742b4e673072066f"
  instance_type = "t2.micro"
  tags = {
    Name = "my-us10-ec2-instance"
    terraform = "true"
  }
  provider = aws.my-us10-account-profile
}