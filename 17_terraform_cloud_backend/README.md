# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
- Please refer [here](../01_first-ec2/README.md).

# Terraform Cloud
- Terraform Cloud is an application that helps teams use Terraform together. It manages Terraform runs in a consistent and reliable environment, and includes easy access to shared state and secret data, access controls for approving changes to infrastructure, a private registry for sharing Terraform modules, detailed policy controls for governing the contents of Terraform configurations, and more.
- For more information, please refer [here](https://learn.hashicorp.com/collections/terraform/cloud-get-started?utm_source=WEBSITE&utm_medium=WEB_IO&utm_offer=ARTICLE_PAGE&utm_content=DOCS).

# Sentinel
- Sentinel is an embedded policy-as-code framework integrated with HashiCorp Enterprise products.  It enables fine-grained, logic-based policy decisions, and can be extended to use information from external sources.
- Sentinel policy is a paid feature of Terraform cloud.
- For more information on sentinel policy, please refer [here](https://www.terraform.io/docs/cloud/sentinel/index.html).

# Terraform Cloud: Remote Backend
- Terraform Cloud can also be used with local backend operations, in which case only state is store in the Terraform Cloud Backend.
- When using full remote operations, operations like terraform plan or terraform apply can be executed in Terraform Cloud's run environment, with log output streaming to the local terminal.
- Terraform Cloud supports a remote backend and this backend type is known as Enhanced Remote Backend. For more information about this backend, please refer [here](https://www.terraform.io/docs/language/settings/backends/remote.html).
- Terraform cloud remote backend configurations are defined in [backend.hcl](backend.hcl) file. The token for terraform cloud is generated using `terraform login` command. `terraform login` command will store the entered token in `$HOME/.terraform.d/credentials.tfrc.json` file. Then run:
    ```
    terraform init -backend-config=backend.hcl
    ```
  Then run command `terraform plan`. You can see the remote plan run logs locally, so you will also be able to see the logs of cost estimation and policy checks provided these features are enabled.
