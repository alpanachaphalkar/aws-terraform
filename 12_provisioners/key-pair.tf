resource "aws_key_pair" "login-key" {
  key_name   = "login-key"
  public_key = file("$HOME/.ssh/my-key.pub")
}

// To generate a Key-Pair locally run following commands:
// ssh-keygen -m PEM && mv my-key.pub $HOME/.ssh/my-key.pub && mv my-key $HOME/.ssh/my-key
// chmod 400 $HOME/.ssh/my-key