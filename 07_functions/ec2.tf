resource "aws_instance" "my-ec2" {
  ami           = lookup(var.ami, var.aws_region)
  instance_type = "t2.micro"
  key_name      = aws_key_pair.login-key.key_name
  count         = 2
  tags = {
    Name = element(var.tags, count.index)
  }
}