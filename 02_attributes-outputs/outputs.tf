output "myS3Arn" {
  value = aws_s3_bucket.myS3.arn
}

output "myS3" {
  value = aws_s3_bucket.myS3
}

output "eip" {
  value = aws_eip.lb.public_ip
}