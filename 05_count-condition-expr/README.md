# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Count Parameter
- The count parameter on resources can simplify configurations and let you scale resources by simply incrementing a number.
- For example, refer [here](ec2.tf)

# Conditional Expression
A conditional expression uses the value of a bool expression to select one of the two values.

Syntax:
```
condition ? true_val : false_val
```
If condition is true then the result is true_val. If condition is false then the result if false_val.