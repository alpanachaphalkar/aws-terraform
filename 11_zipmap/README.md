# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Zipmap function
- Zipmap documentation is accessible [here](https://www.terraform.io/docs/language/functions/zipmap.html).
- For example, refer [outputs.tf](outputs.tf).