provider "aws" {
  region = var.aws_region
  // access_key --> set by env var AWS_ACCESS_KEY_ID
  // secret_key --> set by env var AWS_SECRET_ACCESS_KEY
}