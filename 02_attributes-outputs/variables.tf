variable "aws_region" {
  // value can be overridden with env var AWS_DEFAULT_REGION
  default = "eu-central-1"
  type    = string
}

variable "instance_type" {
  // value can be overridden with env var TF_VAR_instance_type
  type = string
  default = "t2.micro"
}