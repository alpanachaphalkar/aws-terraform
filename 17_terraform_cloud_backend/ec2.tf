resource "aws_instance" "my-ec2" {
  ami           = "ami-0db9040eb3ab74509"
  instance_type = "t2.micro"
  tags = {
    Name      = "my-ec2-instance"
    terraform = "true"
  }
}
