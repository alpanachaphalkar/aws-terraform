resource "aws_instance" "myEc2" {
  ami           = var.ami_list[1]
  instance_type = var.instance_type_map.eu-central-1

  tags = {
    Name = "HelloWorld"
  }
}