# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).
- AWS resource "aws_key_pair" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Functions
- Terraform includes a number of built-in functions that you can use to transform and combine values.
- General Syntax:
    ```
    function_name(argument1, argument2)
    ```
- Terraform does not support user-defined functions, and so only the built-in functions are available for use. Following are some categories for which built-in functions are available for use:
    
    + Numeric
    + String
    + Collection
    + Encoding
    + Filesystem
    + Date and Time
    + Hash and Crypto
    + IP Network
    + Type Conversion
    
- For more information please refer [official documentation](https://www.terraform.io/docs/language/functions/index.html).
- To try/test the built-in functions on command line, use following command:
    ```
    terraform console
    ```
- How to use functions? - Demonstrated in [ec2.tf](ec2.tf), [key-pair.tf](key-pair.tf), [locals.tf](locals.tf) files.
