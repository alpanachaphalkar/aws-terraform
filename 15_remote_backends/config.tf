terraform {
  required_version = "> 0.12.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.34.0"
    }
  }

  // Backend config is set via CLI, but partial backend config must be defined.
  backend "s3" {
    bucket         = "s3-terraform-remote-backend"
    dynamodb_table = "s3-terraform-state-locking"
  }

  /* // !! Variables are not allowed in Backend Config !!
  backend "s3" {
    bucket = "terraform-remote-backend-alpana"
    key    = "remote-demo.tfstate"
    region = "eu-central-1"
    access_key = "PUT_YOUR_ACCESS_KEY"
    secret_key = "PUT_YOUR_SECRET_KEY"
  }*/
}
