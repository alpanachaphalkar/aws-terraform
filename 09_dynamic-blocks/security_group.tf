resource "aws_security_group" "dynamic_sg" {
  name        = "terraform-security-group"
  description = "Security Groups for Vault"
  /*ingress {
    from_port   = 8200
    protocol    = "tcp"
    to_port     = 8200
    cidr_blocks = []
  }*/

  dynamic "ingress" {
    for_each = var.sg_ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = var.sg_egress_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}