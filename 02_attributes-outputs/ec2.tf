resource "aws_instance" "myEc2" {
  ami           = "ami-0db9040eb3ab74509"
  instance_type = var.instance_type

  tags = {
    Name = "HelloWorld"
  }
}

resource "aws_eip" "lb" {
  vpc = true
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.myEc2.id
  allocation_id = aws_eip.lb.id
}