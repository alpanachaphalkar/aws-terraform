module "prod" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.17.0"

  name = "my-ec2"
  instance_type = "t2.large"
  instance_count = 1
  ami = "ami-0db9040eb3ab74509"
  subnet_id = "subnet-48f82334"
  tags = {
    terraform = "true"
    environment = "prod"
  }
}