resource "aws_instance" "my-ec2" {
  ami           = var.ami
  instance_type = var.instance_type
  count         = 3
  tags = {
    Name = "my-ec2-${count.index}"
  }
}