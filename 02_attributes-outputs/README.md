# Official Documentation
- AWS resource "aws_s3_bucket" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket).
- AWS resource "aws_eip" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip).
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).
- AWS resource "aws_eip_association" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip_association).
- AWS resource "aws_security_group" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Attributes
- Terraform has the capability to output the attribute of the resource with the output values.
- An outputted attributes can not only be used for the user reference, but it can also act as an input to other resources being created via Terraform.