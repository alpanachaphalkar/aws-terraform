terraform {
  required_version = "> 0.12.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.34.0"
    }
  }

  // Token for terraform cloud is generated using terraform login command.
  // Terraform login command will store the entered token in $HOME/.terraform.d/credentials.tfrc.json file.
  backend "remote" {}
}
