resource "aws_key_pair" "login-key" {
  key_name   = "login-key"
  public_key = file("${path.module}/id_rsa.pub")
}