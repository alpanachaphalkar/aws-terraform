resource "aws_instance" "my-ec2" {
  ami           = var.ami
  instance_type = var.instance_type
  count         = 3
  tags = {
    Name = var.ec2_names[count.index]
  }
}

resource "aws_instance" "project-ec2" {
  ami           = var.ami
  instance_type = var.instance_type
  count         = var.isProd == true ? 1 : 0
  tags = {
    Name = "project-ec2-${count.index}"
  }
}