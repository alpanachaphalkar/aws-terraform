# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Data Sources
- Data sources allow data to be fetched or computed for use elsewhere in Terraform configuration.
- For example, data sources are defined in [data.tf](data.tf) and data source is accessed in [ec2.tf](ec2.tf). 