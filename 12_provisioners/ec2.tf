resource "aws_instance" "my-ec2" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = aws_key_pair.login-key.key_name
  security_groups = [aws_security_group.my_ec2_sg.name]
  tags = {
    Name = "my-ec2"
  }

  provisioner "remote-exec" {
    on_failure = continue
    inline = [
      "sudo amazon-linux-extras install -y nginx1.12",
      "sudo systemctl start nginx"
    ]
    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("$HOME/.ssh/my-key")
    }
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.my-ec2.private_ip} >> private_ip.txt"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "echo 'Destroy-time provisioner'"
  }
}