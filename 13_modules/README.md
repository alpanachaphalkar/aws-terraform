# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).
- For Terraform modules, please refer [here](https://www.terraform.io/docs/language/modules/index.html).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Modules
- For custom modules please [refer](modules) and to access these modules, please refer [dev](envs/dev/ec2.tf) and [stage](envs/stage/ec2.tf) resources.

# Supported Module Sources
The `source` argument in a module block tells Terraform where to find the source code for the desired child module.
- Local Paths
- Terraform registry
- GitHub
- Bitbucket
- Generic Git, Mercurial Repositories
- HTTP URLs
- S3 Buckets
- GCS Buckets

For more information on terraform module sources, please refer [here](https://www.terraform.io/docs/language/modules/sources.html).

# Terraform Registry
- [Terraform registry](https://registry.terraform.io/) is a repository of modules written by the Terraform community. 
- Within the terraform registry, you can find verified modules that are maintained by various third party vendors. These modules are available for various resources like AWS VPC, RDS, ELB and others.
- Verified modules are reviewed by HashiCorp and actively maintained by contributors to stay up-to-date and compatible with both terraform and their respective providers.