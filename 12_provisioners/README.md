# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Provisioners
Provisioners are used to execute script on a local or remote machine as part of resource creation or destruction.
For example, refer [ec2.tf](ec2.tf)

## Well-know Provisioners
Most used and well-known provisioners are:
- remote-exec
- local-exec
For more information on other provisioners, please [refer](https://www.terraform.io/docs/language/resources/provisioners/syntax.html).

### Local Exec Provisioner
local-exec provisioners allows us to invoke local executable after resource is created. Well known use case of local-exec provisioner is for executing the ansible playbooks.

### Remote Exec Provisioner
remote-exec provisioners allows us to invoke script directly on the remote server.

## Creation-Time Provisioners
- By default, provisioners run when the resource they are defined within is created. Creation-time provisioners are only run during creation, not during updating or any other lifecycle. They are meant as a means to perform bootstrapping of a system.
- If a creation-time provisioner fails, the resource is marked as tainted. A tainted resource will be planned for destruction and recreation upon the next terraform apply. Terraform does this because a failed provisioner can leave a resource in a semi-configured state.

## Destroy-Time Provisioners
- If `when = destroy` is specified, the provisioner will run when the resource it is defined within is destroyed.
- Destroy provisioners are run before the resource is destroyed. If they fail, Terraform will error and rerun the provisioners again on the next terraform apply.
- For example, refer [ec2.tf](ec2.tf)

## Failure Behavior
By default, provisioners that fail will also cause the Terraform apply itself to fail. The `on_failure` setting can be used to change this. The allowed values are:
- `continue` - Ignore the error and continue with creation or destruction.
- `fail` - Raise an error and stop applying (the default behavior). If this is a creation provisioner, taint the resource.