# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Splat Expressions
- Splat expression allows to list the attributes of all resources, provided resources are of the same type.
- For example, refer [outputs.tf](outputs.tf).