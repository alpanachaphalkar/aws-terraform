# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).
- For Terraform workspaces, please refer [here](https://www.terraform.io/docs/cloud/workspaces/index.html).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Terraform Workspaces
- Terraform allows us to have multiple workspaces, with each of the workspace we can have different set of environment variables associated.
- To create, change and delete terraform workspaces:
    ```
    terraform workspace
    ```
    For more information on workspace commands, please refer [here](https://www.terraform.io/docs/cli/commands/workspace/index.html).
- Whenever a new workspace is created, folder `terraform.tfstate.d` is created with `<workspace_name>` as a subfolder. Inside this workspace folder, all the state files for the resources created in that workspace are stored. For default workspace, the state file will be stored at the root location. 
- For example, please refer [ec2.tf](ec2.tf).
