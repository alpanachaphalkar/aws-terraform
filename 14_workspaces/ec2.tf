resource "aws_instance" "my-ec2" {
  ami           = "ami-0db9040eb3ab74509"
  instance_type = lookup(var.instance_type, terraform.workspace)
  tags = {
    Name        = "${local.environment}-ec2-instance"
    terraform   = "true"
    environment = local.environment
  }
}

locals {
  environment = lookup(var.environment, terraform.workspace)
}