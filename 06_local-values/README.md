# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Local Values
- A local value assigns a name to an expression, allowing it to be used multiple times within a module without repeating it.
- For example, locals are defined [here](locals.tf) and locals are accessed [here](ec2.tf). 
- Local values can be helpful to avoid repeating the same values or expressions multiple times in a configuration.
- If overused they can also make a configuration hard to read by future maintainers by hiding the actual values used.
- Use local values in moderation, in situations where a single value or result is used in many places and that value is likely to be changed in the future.