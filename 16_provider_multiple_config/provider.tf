provider "aws" {
  region = var.aws_region
  // access_key --> set by env var AWS_ACCESS_KEY_ID
  // secret_key --> set by env var AWS_SECRET_ACCESS_KEY
}

provider "aws" {
  alias = "my-eu20"
  region = "eu-west-2"
}

provider "aws" {
  alias = "my-us10-account-profile"
  region = "us-east-1"
  profile = "my-us10"
}