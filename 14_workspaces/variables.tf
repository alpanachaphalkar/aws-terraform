variable "aws_region" {
  // value can be overridden with env var TF_VAR_aws_region
  default = "eu-central-1"
  type    = string
}

variable "aws_access_key" {
  // override value with env var TF_VAR_aws_access_key
  default = null
  type    = string
}

variable "aws_secret_key" {
  // override value with env var TF_VAR_aws_secret_key
  default = null
  type    = string
}

variable "instance_type" {
  type = map(string)
  default = {
    default = "t2.nano"
    dev     = "t2.micro"
    prod    = "t2.medium"
  }
}

variable "environment" {
  type = map(string)
  default = {
    default = "default"
    dev     = "development"
    prod    = "production"
  }
}