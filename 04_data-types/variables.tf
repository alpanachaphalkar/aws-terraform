variable "aws_region" {
  // value can be overridden with env var TF_VAR_aws_region
  default = "eu-central-1"
  type    = string
}

variable "aws_access_key" {
  // override value with env var TF_VAR_aws_access_key
  default = null
  type    = string
}

variable "aws_secret_key" {
  // override value with env var TF_VAR_aws_secret_key
  default = null
  type    = string
}

variable "instance_type_map" {
  type = map(string)
  default = {
    us-east-1    = "t2.medium"
    eu-central-1 = "t2.micro"
    eu-west-2    = "t2.nano"
  }
}

variable "ami_list" {
  type    = list(string)
  default = ["ami-0db9040eb3ab74511", "ami-0db9040eb3ab74509", "ami-0db9040eb3ab74533"]
}

variable "elb_name" {
  //override value with terraform.tfvars file
  type = string
}

variable "availability_zones" {
  //override value with terraform.tfvars file
  type = list(string)
}

variable "timeout" {
  //override value with terraform.tfvars file
  type = number
}