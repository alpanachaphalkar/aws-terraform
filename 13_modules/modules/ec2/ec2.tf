resource "aws_instance" "my-ec2" {
  ami             = "ami-0db9040eb3ab74509"
  instance_type   = var.instance_type
  tags = {
    Name = var.instance_name
    terraform = "true"
    environment = var.environment
  }
}