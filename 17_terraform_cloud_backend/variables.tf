variable "aws_region" {
  // value can be overridden with env var TF_VAR_aws_region
  default = "eu-central-1"
  type    = string
}