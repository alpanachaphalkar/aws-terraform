# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).
- Github resources documentation is accessible [here](https://registry.terraform.io/providers/integrations/github/latest/docs).

# Commands
- To download the providers/plugins:
    ```
    terraform init
    ```
- To upgrade to the latest acceptable version of each provider, run:
  ```
  terraform init -upgrade
  ```
- Dry run to see resources being provisioned:
    ```
    terraform plan
    ```
- To provision resources defined in .tf files:
    ```
    terraform apply
    ```
- To provision resources defined in .tf files without interactive approval:
    ```
    terraform apply -auto-approve
    ```
- To de-provision resources created as per the .tfstate file:
    ```
    terraform destroy -auto-approve
    ```
- To de-provision specific resource:
    ```
    terraform destroy -target <resource_type>.<resource_name>
    ```
    ```
    terraform destroy -target aws_instance.myEc2
    ```
- To de-provision specific resource, the resource definition in .tf file can be commented out or removed and after that use `terraform apply` command.
- To format the code in terraform configuration files:
    ```
    terraform fmt
    ```
- To validate whether terraform configurations are syntactically correct:
    ```
    terraform validate
    ```
- To taint terraform-managed resource manually in order to forcefully destroy the resource and recreate the resource in next `terraform apply`, run:
    ```
    terraform taint <resource_type>.<resource_name>
    ```
    ```
    terraform taint aws_instance.myEc2
    ```
  This command only marks the resource tainted in `.tfstate` file. Whenever `terraform apply` command is executed after tainting the resource, resource will be destroyed and recreated.
- To generate the visual representation of a configuration or execution plan:
    ```
    terraform graph
    ```
  The generated file is in `.dot` format which can be converted to an image(`.jpg`, `.png`) format using [graphviz](https://graphviz.org/download/) tool:
    ```
    dot -Tpng graph.dot -o graph.png
    ```
  For example, [graph.dot](../07_functions/graph.dot) file and converted [graph.png](../07_functions/graph.png) file.
- To get the value of the outputs defined in `.tf` files:
    ```
    terraform output <output_name>
    ```
  Based on output defined in [outputs.tf](../10_splat-expression/outputs.tf) file, run:
    ```
    terraform output arns
    ```
- To create, change and delete terraform workspaces:
    ```
    terraform workspace
    ```
  For more information on workspace commands, please refer [here](https://www.terraform.io/docs/cli/commands/workspace/index.html).

# State file
- Terraform stores the state of the infrastructure that is being created from the TF files.
- This state allows Terraform to map real world resource to your existing configurations.
- The state file will be created/modified on `terraform apply` command and this file has extension `.tfstate`.

# Desired and Current State
- Terraform's primary function is to create, modify, and destroy infrastructure resources to match the desired state described in a Terraform configuration (`.tf` file).
- Current state is the actual state of a resource that is currently being deployed.
- If there is a difference between the current state & desired state, `terraform plan` command presents a description of the required changes to achieve the desired state.
- To get the current state of the deployed resources, use command:
    ```
    terraform refresh
    ```
  This command updates the state file which describes the current state of the resources.
- `terraform plan` command internally runs `terraform refresh` command to describe the required changes to achieve the desired state.

# Dependency Lock File
- Terraform dependency lock file allows us to lock to a specific version of the provider.
- If a particular provider already has a selection recorded in the lock file, Terraform will always re-select that version for installation, even if a newer version has become available.
- You can override that behavior by running following command:
  ```
  terraform init -upgrade
  ```