module "stage" {
  source = "../../modules/ec2"
  instance_type = "t2.medium"
  environment = "stage"
  instance_name = "stage-ec2"
}