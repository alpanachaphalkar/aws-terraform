variable "aws_region" {
  // value can be overridden with env var TF_VAR_aws_region
  default = "eu-central-1"
  type    = string
}

variable "aws_access_key" {
  // override value with env var TF_VAR_aws_access_key
  default = null
  type    = string
}

variable "aws_secret_key" {
  // override value with env var TF_VAR_aws_secret_key
  default = null
  type    = string
}

variable "tags" {
  type    = list(string)
  default = ["first-ec2", "second-ec2"]
}

variable "ami" {
  type = map(string)
  default = {
    "us-east-1"    = "ami-0db9040eb3ab74ue1"
    "eu-west-2"    = "ami-0db9040eb3ab74ew2"
    "eu-central-1" = "ami-0db9040eb3ab74509"
  }
}