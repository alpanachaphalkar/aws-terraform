resource "aws_ebs_volume" "db_ebs" {
  availability_zone = "${var.aws_region}b"
  size              = 8
  tags              = local.common_tags
}