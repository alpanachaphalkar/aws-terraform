terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
  // access_key --> set by env var AWS_ACCESS_KEY_ID
  // secret_key --> set by env var AWS_SECRET_ACCESS_KEY
}