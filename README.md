# Terraform
This repository contains code to demonstrate [Terraform](https://www.terraform.io/) for AWS infrastructure provisioning.

### Official Documentation
- Terraform binary can be downloaded from [here](https://www.terraform.io/downloads.html).
- Terraform docs is accessible [here](https://www.terraform.io/docs/index.html).
- Terraform providers documentation is accessible [here](https://registry.terraform.io/browse/providers).
- Terraform AWS provider documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs).

# Demos
- Getting started: [Terraform with AWS](https://youtu.be/SicUXKDRT_Y).
- [Terraform Remote Backend: S3](https://youtu.be/QqqCXp9z_c0).
- [Terraform Cloud](https://youtu.be/BzOwr7H0e5M).
- [Terraform with AWS SAM](https://youtu.be/Z0TVNdgiqvw).
