# Official Documentation
- AWS resource "aws_security_group" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Dynamic Blocks
- Dynamic blocks allows us to dynamically construct repeatable nested blocks which is supported inside resource, data, provider and provisioner blocks.
- Dynamic block usage is demonstrated [here](security_group.tf).