module "dev" {
  source = "../../modules/ec2"
  instance_type = "t2.micro"
  environment = "dev"
  instance_name = "dev-ec2"
}