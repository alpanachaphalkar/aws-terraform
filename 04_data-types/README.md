# Official Documentation
- AWS resource "aws_elb" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/elb).
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Data Types
- The type argument in a [variable block](variables.tf) allows you to restrict the type of value that will be accepted as the value for a variable.
- If no type constraints is set then a value of any type is accepted.
- Following are the data types:
  
    | Types | Description |
    |-------|-------------|
    |string| Sequence of unicode chars representing some text, for e.g. "hello" |
    |list| Sequential list of values identified by their position. Starts with 0. For e.g. ["Munich", "Mumbai", "London"]
    |map| A group of values identified by named labels, for e.g. {name = "Alpana", age = 29}
    |number| For e.g. 200 |