variable "aws_region" {
  // value can be overridden with env var TF_VAR_aws_region
  default = "eu-central-1"
  type    = string
}

variable "aws_access_key" {
  // override value with env var TF_VAR_aws_access_key
  default = null
  type    = string
}

variable "aws_secret_key" {
  // override value with env var TF_VAR_aws_secret_key
  default = null
  type    = string
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ami" {
  type    = string
  default = "ami-0db9040eb3ab74509"
}