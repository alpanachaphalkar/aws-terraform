# Official Documentation
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
- Please refer [here](../01_first-ec2/README.md).
- For more information on `terraform state` command, please refer [here](https://www.terraform.io/docs/cli/commands/state/index.html).

# Remote Backends
- Since `.tfstate` files might contain sensitive data, hence it is recommended to use remote backends for storing the `.tfstate` files rather than pushing it to any git repository.
- Supported Remote Backends: S3, consul, etcd, GCS, artifactory, azurerm, etc.
- For more information on remote backends, please refer [here](https://www.terraform.io/docs/language/settings/backends/index.html).
- Remote backend configuration can be dynamically passed from CLI command:
    ```
     terraform init \
      -backend-config="region=eu-central-1" \
      -backend-config="key=project/dev/branch-name.tfstate"
    ```
  Although config is set via CLI, partial backend config must be defined, as shown below:
  ```
  terraform {
    backend "s3" {
      bucket = "s3-terraform-remote-backend"
      dynamodb_table = "s3-terraform-state-locking"
    }
  }
  ```

# State lock
- Whenever you are performing write operation, terraform would lock the state file. This is very important as otherwise during your ongoing terraform apply operations, if others also try for the same, it would corrupt your state file. For S3 remote backend, state locking is supported via Dynamo DB.
- For more information, please refer [here](https://www.terraform.io/docs/language/settings/backends/s3.html).

# State management
- As your terraform usage becomes more advanced, there are some cases where you may need to modify the terraform state.
- It is important to never modify the state file directly. Instead, make use of terraform state command.
- For state modification, use `terraform state` command. For more information on `terraform state` command, please refer [here](https://www.terraform.io/docs/cli/commands/state/index.html).

# Import
- Terraform is able to import existing infrastructure. This allows you take resources you've created by some other means and bring it under Terraform management.
- For more information on `terraform import` command, please refer [here](https://www.terraform.io/docs/cli/commands/import.html).
