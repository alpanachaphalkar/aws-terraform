output "ec2_map" {
  value = zipmap(aws_instance.my-ec2[*].tags.Name, aws_instance.my-ec2[*].arn)
}