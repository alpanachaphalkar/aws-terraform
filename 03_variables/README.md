# Official Documentation
- AWS resource "aws_security_group" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group).
- AWS resource "aws_instance" documentation is accessible [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance).

# Commands
Please refer [here](../01_first-ec2/README.md).

# Variable Assignments
Variable in Terraform can be assigned values in multiple ways. Some of these include:

- Environment Variables
- Command Line Flags
- From a File
- Variable Defaults

### Variables: Command Line Flags
To assign the value to a variable from command line, run:
```
terraform apply -var="instance_type=t2.nano"
```

### Variables: Variable Defaults
Please refer [here](variables.tf)

### Variables: From a File
- Variables are defined in `.tfvars` file like [this](terraform.tfvars). Then run `terraform apply`. The variable value defined in  [terraform.tfvars](terraform.tfvars) file will be assigned.
- In case of multiple `.tfvars` files, terraform will assign the value to a variable from `terraform.tfvars` file if that file exists and it will ignore other `.tfvars` files.
- To assign variables from a file other than `terraform.tfvars` file like [prod.tfvars](prod.tfvars), run:
    ```
    terraform apply -var-file="prod.tfvars"
    ```
  
### Variables: Environment Variables
- To assign a value via environment variables, create environment variable of format `TF_VAR_<variable_name>`.
- For example, to assign the value to a variable `instance_type`, create environment variable `TF_VAR_instance_type` with value "m5.large". Then run:
    ```
    terraform apply
    ```
### Priorities with Variable Assignments
Terraform will assign or override the values to variables with following priorities:
1. Variables with command line flags either from `.tfvars` file or directly.
2. File `terraform.tfvars`
3. Environment Variables
4. Variable defaults

If the variables are declared in `.tf` file, but they are not assigned values via any of the ways mentioned above, then value must be specified during `terraform apply` command user-interaction, if not specified, terraform will throw error.