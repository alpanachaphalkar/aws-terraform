variable "instance_type" {
  type    = string
  default = "t2.nano"
}

variable "instance_name" {
  type = string
}

variable "environment" {
  type = string
}